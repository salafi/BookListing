package com.example.ibnshahid.booklisting;

import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ibnShahid on 27/07/2017.
 */

public class QueryUtils {
    /**
     * Return a list of {@link BookModel} objects that has been built up from
     * parsing a JSON response.
     */
    public static List<BookModel> extractBooks(String json) {
        List<BookModel> books = new ArrayList<>();
        try {
            // TODO: Parse the response given by the SAMPLE_JSON_RESPONSE string and
            JSONObject root = new JSONObject(json);
//            int book_numbers = root.getInt("totalItems");
            JSONArray response = root.getJSONArray("items");
            for (int i=0; i<response.length(); i++) {
                JSONObject res = response.getJSONObject(i);
                Log.e("res", ""+res);
                JSONObject volumeInfo = res.getJSONObject("volumeInfo");
                String title = volumeInfo.getString("title");
                String authors = volumeInfo.getString("authors");
                BookModel book = new BookModel();
                book.title = title;
                book.authors = authors;
                books.add(book);
            }

           // ----------------------------------------------------------------------
//            Log.e("book number", "" + book_numbers);
//            Log.e("response", ""+response.get(0));
//            JSONArray results = response.getJSONArray("results");
//            for (int i=0; i<results.length(); i++) {
//                JSONObject res = results.getJSONObject(i);
//                String title = res.getString("webTitle");
//                String section = res.getString("sectionName");
//                String date = res.getString("webPublicationDate");
//                String url = res.getString("webUrl");
////                BookModel newsPiece = new BookModel(title, section, date, url); //TODO this shit
////                news.add(newsPiece);
//            }

        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the Booklisting JSON results", e);
        }
        return books;
    }

    public static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e("error", "Error response code: " + urlConnection.getResponseCode());
                jsonResponse = "";
            }
        } catch (IOException e) {
            Log.e("error", "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
            if (inputStream != null) inputStream.close();
        }

        return jsonResponse;
    }

    public static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    public static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }
}
