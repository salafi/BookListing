package com.example.ibnshahid.booklisting;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by ibnShahid on 27/07/2017.
 */

public class BookLoader extends AsyncTaskLoader<List<BookModel>>{

    private String mUrl;

    public BookLoader(Context context, String url) {
        super(context);
        Log.e("url", url);
        mUrl = url;
    }

    @Override
    public List<BookModel> loadInBackground() {
        if (mUrl == null) return null;
        URL url = QueryUtils.createUrl(mUrl);
        String jsonResponse = "";
        try {
            jsonResponse = QueryUtils.makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return QueryUtils.extractBooks(jsonResponse);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
