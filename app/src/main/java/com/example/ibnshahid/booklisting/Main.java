package com.example.ibnshahid.booklisting;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class Main extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<BookModel>> {

    BookAdapter adapter = null;
    ListView bookListView = null;
    List<BookModel> data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle = new Bundle();
        getLoaderManager().initLoader(1, bundle, this);

        bookListView = (ListView) findViewById(R.id.ll_list);
    }

    String getUrl (String request) {
        String url = Constants.test + Constants.apiKey;
        return url;
//        return "https://www.googleapis.com/books/v1/volumes?q=android&maxResults=1";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        return true;
    }

    @Override
    public Loader<List<BookModel>> onCreateLoader(int id, Bundle args) {
        String url = getUrl(args.getString("request"));
        Log.e("url", url);
        return new BookLoader(this, url);
    }

    @Override
    public void onLoadFinished(Loader<List<BookModel>> loader, final List<BookModel> data) {
        this.data = data;
        adapter = new BookAdapter(this, 0, this.data);
        bookListView.setAdapter(adapter);
        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = data.get(position).url;
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<List<BookModel>> loader) {

    }

}
